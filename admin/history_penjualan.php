<?php include "header.php";?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        History Penjualan
      </h1>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-body">
          <div class="col-md-6">
            <div class="row">
              <div class="form-group">
              <label for="exampleInputDate">Tanggal Awal</label>
              <div class="input-group date">
              <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
              </div>
              <input type="text" autocomplete="off" class="form-control pull-right" id="StartDate" name="StartDate" data-error="Tanggal Tidak Boleh Kosong" required>
              <div class="help-block with-errors"></div>
              </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="row">
              <div class="form-group">
              <label for="exampleInputDate">Tanggal Akhir</label>
              <div class="input-group date">
              <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
              </div>
              <input type="text" autocomplete="off" class="form-control pull-right" id="EndDate" name="EndDate" data-error="Tanggal Tidak Boleh Kosong" required>
              <div class="help-block with-errors"></div>
              </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
          <div class="form-group">
            <button type="button" class="btn btn-primary" id="btnSearchHistory" name="btnSearchHistory"> Search </button>
          </div>
          </div>
          <table id="THistory" class="table table-bordered table-striped dt-responsive" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>No Faktur</th>
                <th>Tanggal</th>
                <th>Nama Pelanggan</th>
                <th>Qty</th>
                <th>Harga Modal</th>
                <th>Harga Jual</th>
                <th>Bersih</th>
                <th>Deskripsi</th>
              </tr>
            </thead>
            <tfoot>
            <tr>
                <th colspan="7">Total</th>
                <th></th>
            </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </section>
  </div>
<?php include "footer.php";?>
<script type="text/javascript">
  $(document).ready(function() {
    var currDate = new Date();
    $('#StartDate').datepicker({
     autoclose: true,
     //defaultDate: currDate
   })

   $('#EndDate').datepicker({
    defaultDate: currDate,
     autoclose: true
   })
var table = $('#THistory').DataTable({
			"bProcessing": true,
      "dom": 'Bflipt',
      "paging":   false,
      "serverSide": true,
      "responsive": true,
      "buttons": 
      [
        { extend: 'pdfHtml5', footer: true }
      ],
      "footerCallback": function ( row, data, start, end, display ) {
        var api = this.api(), data;
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };
        total = api
            .column( 6 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );
        $( api.column( 7 ).footer() ).html(
            'Rp '+ numeral(total).format('0,0') +''
        );
      },      
      "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        return nRow;
      },

      "ajax": {
                "url": "search_total_transaction_by_date.php",
                "type": "POST",
                "data": function (d) {
                    d.StartDate = $("#StartDate").val(),
                    d.EndDate   = $("#EndDate").val()
                }
            },
    });

    $("#btnSearchHistory").on("click",function(){
      $("#THistory").DataTable().draw();
    });
});
</script>
